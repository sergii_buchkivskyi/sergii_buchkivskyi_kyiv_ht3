package tests;

import org.testng.annotations.Test;
import static org.testng.Assert.assertTrue;

public class LetterToSupportTest extends BaseTest {

    private static final String USER_NAME_FIELD = "testName";
    private static final String USER_EMAIL_FIELD = "test@gmail.com";
    private static final String USER_CONTENT_FIELD = "testContent";

    @Test
    public void checkSendingLetterToSupport(){
        getHomePage().clickLetterToDirectorLink();
        getHomePage().clickSupportMenuItem();
        getHomePage().inputDataToUserNameField(USER_NAME_FIELD);
        getHomePage().inputDataToUserEmailField(USER_EMAIL_FIELD);
        getHomePage().inputDataToContentField(USER_CONTENT_FIELD);
        getHomePage().clickSubmitButton();
        assertTrue(getHomePage().letterWasSentIsDisplayed());
    }

}
