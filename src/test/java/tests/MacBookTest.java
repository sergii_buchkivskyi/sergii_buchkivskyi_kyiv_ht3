package tests;

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;

public class MacBookTest extends BaseTest{

    private static final long TIMEOUT = 30;
    private static final int EXPECTED_AMOUNT_OF_MACBOOK = 2;

    @Test
    public void checkAmountMacBookWithSizeScreenIsEleven(){
        getHomePage().clickAppleStore();
        getAppleStorePage().clickMacBookButton();
        getMacBookPage().clickFilterScreenSizeIsElevenLabel();
        getMacBookPage().implicitWait(TIMEOUT);
        getMacBookPage().clickFilterScreenSizeIsElevenTooltip();
        assertEquals(getMacBookPage().sizeOfMacBookListElements(),EXPECTED_AMOUNT_OF_MACBOOK);
    }

}
