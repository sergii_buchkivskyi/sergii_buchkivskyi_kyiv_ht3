package tests;

import org.testng.annotations.Test;
import static org.testng.Assert.assertTrue;

public class SignUpTest extends BaseTest{

    @Test
    public void checkSignUpWithInvalidPhoneNumber(){
        getHomePage().clickSingInButton();
        getSingInPage().clickSignUpLink();
        getSingUpPage().inputUserPhone();
        getSingUpPage().inputUserPassword();
        getSingUpPage().inputRepeatUserPassword();
        getSingUpPage().clickSubmitSignUpButton();
        assertTrue(getSingUpPage().errorSignUpIsDisplayed());
    }
}
