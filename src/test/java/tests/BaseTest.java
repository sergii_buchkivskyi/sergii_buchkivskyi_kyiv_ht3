package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import pages.*;
import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class BaseTest {
    private WebDriver driver;
    private static final String URL = "https://avic.ua/";

    @BeforeTest
    public void profileSetUp(){
        chromedriver().setup();
    }

    @BeforeMethod
    public void testsSetUp(){
        driver = new ChromeDriver(); //created new sample by chromedriver
        driver.manage().window().maximize(); //open window on fullscreen
        driver.get(URL); //open site
    }

    @AfterMethod
    public void tearDown(){
        driver.close(); //close driver
    }
    public WebDriver getDriver(){return driver;}

    public HomePage getHomePage(){
        return new HomePage(getDriver());
    }
    public MacBookPage getMacBookPage(){
        return new MacBookPage(getDriver());
    }
    public AppleStorePage getAppleStorePage(){return new AppleStorePage(getDriver());}
    public SignUpPage getSingUpPage(){return new SignUpPage(getDriver());}
    public SingInPage getSingInPage(){return new SingInPage(getDriver());}
}
