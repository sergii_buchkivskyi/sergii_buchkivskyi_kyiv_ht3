package tests;

import org.testng.annotations.Test;
import static org.testng.Assert.assertTrue;

public class SortTest extends BaseTest {

    @Test
    public void checkPriceSortingInDescendingOrder() {
        getHomePage().clickAppleStore();
        getAppleStorePage().clickMacBookButton();
        getMacBookPage().clickSortingComboBox();
        getMacBookPage().clickPriceSortingInDescendingOrder();
        assertTrue(getMacBookPage().checkSortingOfMacBookInDescendingOrder());
    }
}
