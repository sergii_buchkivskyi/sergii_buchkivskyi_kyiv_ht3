package pages;

import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage{
    private String letterToDirectorLink = "//*[@class='header-top__item lgreen-color js_addMessage_btn']";
    private String supportMenuItem = "//a[text()='Служба поддержки']";
    private String userNameField = "//*[contains(@data-recipient-name, 'Служба поддержки')]//input[@name='user_name']";
    private String userEmailField = "//*[contains(@data-recipient-name, 'Служба поддержки')]//input[@name='user_email']";
    private String contentField = "//*[contains(@data-recipient-name, 'Служба поддержки')]//textarea[@name='content']";
    private String submitButton = "//*[contains(@data-recipient-name, 'Служба поддержки')]//*[@class='button-reset main-btn main-btn--green submit']";
    private String letterWasSentNotification = "//div[@class='ttl color-green']";
    private String appleStoreButton = ("//ul[contains(@class,'sidebar-list')]//a[contains(@href, 'apple-store')]");
    private String singInButton = "//div[@class='header-bottom__right-icon']//i[@class='icon icon-user-big']";

    public HomePage(WebDriver driver){
        super(driver);
    }

    public void clickLetterToDirectorLink(){
        clickButton(letterToDirectorLink);
    }

    public  void clickSupportMenuItem(){
        clickButton(supportMenuItem);
    }

    public void inputDataToUserNameField(String userName){
        inputText(userNameField, userName);
    }

    public void inputDataToUserEmailField(String userEmail){
        inputText(userEmailField, userEmail);
    }

    public void inputDataToContentField(String content){
        inputText(contentField, content);
    }

    public void clickSubmitButton(){
        clickButton(submitButton);
    }

    public boolean letterWasSentIsDisplayed() {
        return isDisplayed(letterWasSentNotification);
    }

    public void clickAppleStore(){
        clickButton(appleStoreButton);
    }

    public void clickSingInButton(){
        clickButton(singInButton);
    }




}
