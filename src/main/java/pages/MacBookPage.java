package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.List;

public class MacBookPage extends BasePage {
    public MacBookPage(WebDriver driver){
        super(driver);
    }

    private String filterScreenSizeIsElevenLabel = "//label[@for='fltr-diagonal-ekrana-11-6']";
    private String filterScreenSizeIsElevenTooltip = "//div[@class='filter-area js_filter_parent open-filter-tooltip']//span[@class='filter-tooltip-inner']";
    private String macBookCart = "//div[@class='prod-cart height']";
    private String selectSortingComboBox = "//div[@class='category-top']//span[@role='combobox']";
    private String setPriceSortingInDescendingOrder = "//li[contains(@id, 'pricedesc')]";
    private String listSortedOfMacBookInDescendingOrder = "//div[@class='prod-cart__prise-new']";

    public void clickFilterScreenSizeIsElevenLabel(){
        clickButton(filterScreenSizeIsElevenLabel);
    }

    public void clickFilterScreenSizeIsElevenTooltip(){
        clickButton(filterScreenSizeIsElevenTooltip);
    }

    public int sizeOfMacBookListElements(){
        return sizeOfListElements(macBookCart);
    }

    public void clickSortingComboBox(){
        clickButton(selectSortingComboBox);
    }

    public void clickPriceSortingInDescendingOrder(){
        clickButton(setPriceSortingInDescendingOrder);
    }

    public List<WebElement> getListSortedOfMacBookInDescendingOrder(){
        return getListElements(listSortedOfMacBookInDescendingOrder);
    }

    public int sizeOfMacBookSortedInDescendingOrder(){
        return sizeOfListElements(listSortedOfMacBookInDescendingOrder);
    }

    public boolean checkSortingOfMacBookInDescendingOrder(){
        boolean arrayIsSorted = true;
        String[] words;
        int[] arrayElements = new int [sizeOfMacBookSortedInDescendingOrder()];

        for (int i = 0; i < sizeOfMacBookSortedInDescendingOrder(); i++) {
            words = (getListSortedOfMacBookInDescendingOrder().get(i).getText().split("[^0-9]"));
            arrayElements[i] = Integer.parseInt(words[0]);
        }

        for (int j = 1; j < arrayElements.length; j++) {
            if (arrayElements[j-1] < arrayElements[j]){
                arrayIsSorted = false;
                break;
            }
        }
        return arrayIsSorted;
    }

}
