package pages;

import org.openqa.selenium.WebDriver;

public class SingInPage extends BasePage{
    public SingInPage(WebDriver driver){
        super(driver);
    }

    private String signUpButton = "//a[@class='btn-offers']";

    public void clickSignUpLink(){
        clickButton(signUpButton);
    }
}
