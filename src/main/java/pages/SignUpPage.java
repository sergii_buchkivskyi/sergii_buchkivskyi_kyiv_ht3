package pages;

import org.openqa.selenium.WebDriver;

public class SignUpPage extends BasePage{
    public SignUpPage(WebDriver driver){
        super(driver);
    }

    private String userPhoneField = "//input[@class='validate' and @name='phone']";
    private String userPassword = "//input[@data-validate='pass']";
    private String userPasswordRepeat = "//input[@data-validate='pass1']";
    private String submitSignUpButton = "//button[@class='button-reset main-btn js_validate submit main-btn--green']";
    private String errorSignUpNotification = "//div[@class='form-field input-field flex pass error']";

    public void inputUserPhone (){
        inputText(userPhoneField, "1111111111");
    }

    public void inputUserPassword(){
        inputText(userPassword,"123456");
    }

    public void inputRepeatUserPassword(){
        inputText(userPasswordRepeat, "123456");
    }

    public void clickSubmitSignUpButton(){
        clickButton(submitSignUpButton);
    }

    public boolean errorSignUpIsDisplayed(){
        return isDisplayed(errorSignUpNotification);
    }
}
