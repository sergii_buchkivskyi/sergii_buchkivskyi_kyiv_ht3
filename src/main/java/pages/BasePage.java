package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.openqa.selenium.By.xpath;

/**
 * Hello world!
 *
 */
public class BasePage {
    WebDriver driver;

    BasePage(WebDriver driver){
        this.driver = driver;
    }

    public void implicitWait (final long timeout){
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
    }

    public List<WebElement> getListElements(String locator){
        return driver.findElements(xpath(locator));
    }

    public int sizeOfListElements(String locator){
        return getListElements(locator).size();
    }

    public void clickButton(String locator){
        driver.findElement(xpath(locator)).click();
    }

    public void inputText(String locator, String text){
        driver.findElement(xpath(locator)).sendKeys(text);
    }

    public boolean isDisplayed(String locator) {
        return driver.findElement(xpath(locator)).isDisplayed();
    }

}
