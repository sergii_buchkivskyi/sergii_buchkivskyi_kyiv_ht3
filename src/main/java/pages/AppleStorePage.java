package pages;

import org.openqa.selenium.WebDriver;

public class AppleStorePage extends BasePage {
    public AppleStorePage(WebDriver driver){
        super(driver);
    }

    private String macBookButton = "//div[@class='brand-box__info']//img[@alt='MacBook']";

    public void clickMacBookButton(){
        clickButton(macBookButton);
    }






}
